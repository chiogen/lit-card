import { LitElement, html } from "@polymer/lit-element/lit-element";
import { CardStyles, CardStylesHover } from "./lit-card-styles";

interface CardProperties {
	hover: boolean
}

export class Card extends LitElement {

	public static get properties() {
		return {
			hover: Boolean
		}
	}

	protected _render(props: CardProperties) {

		let cssCardHover = props.hover
			? CardStylesHover
			: "";

		return html`
				 ${CardStyles} ${cssCardHover}
				<slot></slot>
        `
	}

}

customElements.define("x-card", Card);
