# @rammbulanz/lit-card

## Install

`yarn add @rammbulanz/lit-card`<br>
`npm install --save @rammbulanz/lit-card`

## Usage

```js

// If you want to use the lit-card element
import '@rammbulanz/lit-card/lit-card'

// If you want to use the styles on another element
import { CardStyles } from '@rammbulanz/lit-card/lit-card-styles'

```